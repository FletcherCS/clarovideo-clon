import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './components/Home';
import Details from './components/Details';
import Result from './components/Result';
import Login from './components/Login';

class App extends Component {
  render() {
    return (
      <Router>
        <div className='App'>
          <Route exact path='/' component={Login} />
          <Route path='/home' component={Home}/>
          <Route path='/details/:movie' component={Details} />          
          <Route path='/result/:result' component={Result} />          
        </div>
      </Router>

    );
  }
}

export default App;
