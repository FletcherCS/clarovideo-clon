import React, { Component } from 'react';
import '../styles/Header.css';
import logo from '../styles/img/claro-video.png';
import { Grid, Col, Row} from 'react-bootstrap';
import { Link } from 'react-router-dom';
class Header extends Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            value: '',
        }
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        console.log(this.state.value);
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <Grid>
                    <Row className="show-grid">
                        <Link to="/home">
                            <Col xs={12} sm={8} md={8}>
                                <img src={logo} alt={'logo'} className="logo" />
                            </Col>
                        </Link>
                        <Col xs={12} sm={4} md={4}>
                            <form onSubmit={this.handleSubmit} className='form-search'>
                                <input type="text" value={this.state.value} onChange={this.handleChange} placeholder='Buscar película' className='input-box' />
                                <Link to={`/result/${this.state.value}`}>
                                    <input type="submit" value="Buscar" className='input-button' disabled={!this.state.value}/>
                                </Link>
                            </form>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Header;