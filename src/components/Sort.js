import React, {Component} from "react";
import { DropdownButton, MenuItem } from 'react-bootstrap';
import '../styles/Sort.css';

class Sort extends Component{
    constructor(){
        super();
        this.onSelect= this.onSelect.bind(this);
        this.state ={
            value: ''
        }
    }

    onSelect(event){
        this.setState({ value: event});
    }

    render(){
        console.log(this.state.value);
        return (
            <div className="Sort">
                <DropdownButton title="Ordenar" pullRight onSelect={this.onSelect}>
                    <MenuItem eventKey="1">Lo más nuevo</MenuItem>
                    <MenuItem eventKey="2">Lo más visto</MenuItem>
                    <MenuItem eventKey="3">Lo más votado</MenuItem>
                    <MenuItem eventKey="4">Ascendente</MenuItem>
                    <MenuItem eventKey="5">Descendente</MenuItem>
                </DropdownButton>
            </div>
        );

    }
}

export default Sort;