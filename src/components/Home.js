import React, { Component } from 'react';
import '../styles/Home.css';

import Header from './Header';
import Sort from './Sort';
import Category from './Category';

class Home extends Component {
    render() {
        return (
            <div>
                <div className="Home">
                    <Header />
                    <Sort />
                </div>
                <div>
                    <Category />
                </div>
            </div>
        );
    }
}



export default Home;
