import React, { Component } from 'react';
import '../styles/Result.css';
import logo from '../styles/img/claro-video.png';
import PropTypes from 'prop-types';
import Item from './Item';
import Header from './Header';
import Sort from './Sort';

class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            result: this.props.match.params.result,
            mounted: false,
            results: false
        }
    }

    loadContent = () => {
        const requestUrl = `https://api.themoviedb.org/3/search/movie?api_key=7dcffa45eb6b1d3810ff8e19e8a9a0a5&language=es-ES&query=${this.state.result}&page=1&include_adult=false`;

        fetch(requestUrl)
            .then(response => response.json())
            .then(data => this.setState({ data }))
            .catch(error => error);
    }


    componentWillReceiveProps({ url }) {
        if (url !== this.props.url && url !== '') {
            this.setState({
                url,
                mounted: true
            }, this.loadContent)
        }
    }

    componentDidMount() {
        if (this.props.url !== '') {
            this.loadContent()
            this.setState({
                mounted: true,
            })
            if (this.state.data.total_results > 0){
                console.log('si hay resultados mayores');
                this.setState({results: true});
            }else{
                this.setState({results: false});
            }
        }
    }

    render() {
        let titleList = ''
        const results = this.state.data.results
        if (results) {
            titleList = results.map((title, i) => {
                let component = <div key={title.id}></div>
                if (i < 5) {
                    const backDrop = `http://image.tmdb.org/t/p/original${title.backdrop_path}`;
                    component = (
                        <Item key={title.id} id={title.id} backdrop={backDrop} />
                    )
                }
                return component
            })
        }
        
        return (
            <div>
                <div className="Home">
                    <Header />
                    <Sort />
                </div>



                {!this.state.results ? 
                    <div ref="titlecategory" className="TitleList" data-loaded={this.state.mounted}>
                        <div className="Title">
                            <h1 className='title-sci'>Resultados de búsqueda</h1>
                            <div className="titles-wrapper" >
                                {titleList}
                            </div>
                        </div>
                    </div>
                :
                    <div ref="titlecategory" className="TitleList" data-loaded={this.state.mounted}>
                        <div className="Title">
                            <h1 className='title-sci'>Sin resultados de búsqueda</h1>
                            <div className="titles-wrapper" >
                                <h1>Sin resultados</h1>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

Result.propTypes = {
    match: PropTypes.any
}

export default Result;