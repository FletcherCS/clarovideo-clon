import React, { Component } from 'react'
import Item from './Item';
import '../styles/Category.css';

class Category extends Component {
    state = {
        data: [],
        mounted: false
    }

    loadContent = () => {
        const requestUrl = 'https://api.themoviedb.org/3/genre/878/movies?api_key=7dcffa45eb6b1d3810ff8e19e8a9a0a5&language=es-ES&include_adult=false&sort_by=created_at.asc';
        fetch(requestUrl)
            .then(response => response.json())
            .then(data => this.setState({ data }))
            .catch(err => console.log("There has been error"))
    }

    componentWillReceiveProps({ url }) {
        if (url !== this.props.url && url !== '') {
            this.setState({
                url,
                mounted: true
            }, this.loadContent)
        }
    }

    componentDidMount() {
        if (this.props.url !== '') {
            this.loadContent()
            this.setState({
                mounted: true
            })
        }
    }

    render() {
        let titleList = ''
        const results = this.state.data.results
        if (results) {
            titleList = results.map((title, i) => {
                let component = <div key={title.id}></div>
                if (i < results.length) {
                    const backDrop = `http://image.tmdb.org/t/p/original${title.backdrop_path}`;
                    component = (
                        <Item key={title.id} id={title.id} backdrop={backDrop}/>
                    )
                }
                return component
            })
        }

        return (
            <div ref="titlecategory" className="TitleList" data-loaded={this.state.mounted}>
                <div className="Title">
                    <h1 className='title-sci'>Ciencia Ficción</h1>
                    <div className="titles-wrapper" >
                        {titleList}
                    </div>
                </div>
            </div>
        )
    }
}

export default Category;
