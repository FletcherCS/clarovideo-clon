import React, { Component } from 'react';
import '../styles/Login.css';
import logo from '../styles/img/claro-video.png';

import { Redirect } from 'react-router-dom';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            isLogged: false
        }

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        if (this.state.username === "fletcher" && this.state.password === "1234") {
            this.setState({ username: '', password: '', isLogged: true });
        } else {
            this.setState({ username: '', password: '', isLogged: false });
        }
    }

    render() {
        return (
            <div className="Login">
                {this.state.isLogged ?
                    <Redirect to="/home" />
                    :
                    <form onSubmit={this.onSubmit} className={'form-login'}>
                        <div className="container-login">
                            <div>
                                <img src={logo} alt="logo" className={'img-login'}/>
                            </div>
                            <label htmlFor={"uname"} className={'login-label'}>Username</label>
                    <input type={"text"}
                                placeholder={"Ingresa usuario"}
                                name={"uname"}
                                value={this.state.username}
                                onChange={(event) => { this.setState({ username: event.target.value }) }}
                                required className={'input-login'}/>
                            <label htmlFor={"psw"} className={'login-label'}>Password</label>
                    <input type={"password"}
                                placeholder={"Ingresa password"}
                                name={"psw"}
                                value={this.state.password}
                                onChange={(event) => { this.setState({ password: event.target.value }) }}
                                required className={'input-login'}/>
                            <button type={"submit"} className={'button-login'}>Iniciar sesión</button>
                            <label />
                            <input type={"checkbox"} name={"remember"}/>
                            <label className={'login-label'}> Recuerdame </label>
                </div>
                        <div className={"container-login"}>
                            <span className={"psw"}>¿Olvidaste tu <a href="#" className={'a-login'}>contraseña?</a></span>
                        </div>
                    </form>
                }
            </div>
        );
    }
}

export default Login;
